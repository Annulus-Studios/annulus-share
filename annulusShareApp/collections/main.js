// Import ostrio:files lib
import { FilesCollection } from 'meteor/ostrio:files';

Posts = new Mongo.Collection('posts');
// Export FilesCollection instance, so it can be imported in other files
export const Images = new FilesCollection({
  collectionName: 'Images',
  allowClientCode: false, // Disallow remove files from Client
  onBeforeUpload(file) {
    // Allow upload files under 20MB, and only in png/jpg/jpeg formats
    if (file.size <= 20485760 && /jpg|jpeg/i.test(file.extension)) {
      if (this.userId) {
          return true;
      } else {
        return 'Please log in. Only logged in user may upload.';
      }
    } else {
      return 'Please upload a JPG/JPEG image with size equal or less than 20MB';
    }
  },
});
