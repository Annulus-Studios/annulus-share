//Import and load everything
//Components
import './ui/components/fixedNavigationBar/fixedNavigationBar.js';//Fixed navbar html
import './ui/components/loginSignupUI/loginSignupUI.js';//Import Login/Signup component
import './ui/components/homePage/homePage.js';//Import main homepage component
import './ui/components/exifShowcase/exifShowcase.js';//Import exifShowcase component
import './ui/components/profilePage/profilePage.js'; //Profilepage component
import './ui/components/tutorialVideo/tutorialVideo.js'; //Tutorialvideo component
import './ui/components/footer/footer.html'; //Footer component

//Pages
import './ui/pages/home/home.js';
import './ui/pages/profile/profile.js';
import './ui/pages/tutorialPage/tutorialPage.js';
import './ui/pages/legal/privacy_policy.html';
import './ui/pages/legal/terms_of_service.html';

//Routes and Navigation
Router.route('/', function () {
  this.render('page_Home');
  document.title = "Annulus Share | Home";
  document.description = "The photography enthusiasts' home. Image metadata analysis and sharing.";
  GAnalytics.pageview();
});

Router.route('/profile', function () {
  Session.setDefault('postView', "publicPosts");
  Session.setDefault('pubViewLim', 5);
  Session.setDefault('privViewLim', 5);
  Session.setDefault('folViewLim', 5);
  Session.setDefault('favViewLim', 5);
  Session.set('currentPost', 0);
  this.render('profileMain');
  document.title = "Annulus Share | Profile";
  document.description = "Image metadata analysis and sharing. Upload here.";
  GAnalytics.pageview();
  $('#commentModal').modal('hide')
});

// given a url like "/post/5"
Router.route('/post/:_id', function () {
  document.title = "Annulus Share | Profile";
  var params = this.params; // { _id: "5" }
  var id = params._id; // "5"
  Session.set('currentPost', id);
  this.render('profileMain');
  $('#commentModal').modal('show')
});

Router.route('/tutorial', function () {
  this.render('tutorialPage');
  document.title = "Annulus Share | Tutorial";
  document.description = "Learn some of the basics.";
  GAnalytics.pageview();
});

Router.route('/privacy_policy', function () {
  this.render('privacyPolicy');
  document.title = "Annulus Share | Privacy";
  document.description = "Our privacy policy.";
  GAnalytics.pageview();
});

Router.route('/terms_of_service', function () {
  this.render('termsOfService');
  document.title = "Annulus Share | Terms Of Use/Service";
  document.description = "Our Terms of Use/Service policy.";
  GAnalytics.pageview();
});
