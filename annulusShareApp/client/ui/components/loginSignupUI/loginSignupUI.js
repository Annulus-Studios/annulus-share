//Imports and dependencies
import "./loginSignupUI.html";
//Variables
var currentUsername = null;
var changePasswordActivate = false;

//Events for resetPassword
Template.resettingPassword.events({
  //Reset password using token email
  'submit #resetPasswordForm' (event) {
    event.preventDefault();
    var resetPassword = event.target.resetPassword.value;
    var resetPasswordConf = event.target.resetPasswordConf.value;
    if (resetPassword === resetPasswordConf) {
      Accounts.resetPassword (Session.get('resetPasswordToken'), resetPassword,
        function (error) {
          if (error) {
            var message = "There was an error resetting the password: <strong>" + error.reason + ".</strong>";
            $('#resetPasswordFailMessage').html(message);
            $('#resetPasswordFailMessage').collapse('show');
            window.setTimeout(function(){
              $('#resetPasswordFailMessage').collapse('hide');
            }, 2000);
          } else {
            Session.set('resetPasswordToken', null);
            Router.go('/');
          };
        }
      )
    } else {
      var message = "Passwords must match!";
      $('#resetPasswordFailMessage').html(message);
      $('#resetPasswordFailMessage').collapse('show');
      window.setTimeout(function(){
        $('#resetPasswordFailMessage').collapse('hide');
      }, 2000);
    };
  },
});

//Events
Template.loginSignupUI.events({
  //Logout
  'click .logout'(event){
    event.preventDefault();
    Meteor.logout();
    Router.go('/');
  },
  //Login functionality
  'submit #loginForm' (event) {
    event.preventDefault();
    var loginUser = event.target.loginEmail.value;
    var loginPassword = event.target.loginPassword.value;
    //Callback error and catch information to give user feedback on fail.
    Meteor.loginWithPassword(loginUser, loginPassword, function(error) {
      if (Meteor.user()) {
        $('#loginFailMessage').collapse('hide');
      } else {
          var message = "There was an error logging in: <strong>" + error.reason + ".</strong>";
          $('#loginFailMessage').html(message);
          $('#loginFailMessage').collapse('show');
          window.setTimeout(function(){
            $('#loginFailMessage').collapse('hide');
            $('#loginFailMessage').html();
          }, 2000);
      };
      return;
    });
  },
  //Change password event
  'submit #changePassword' (event) {
    event.preventDefault();
    var newPassword = event.target.newPassword.value;
    var oldPassword = event.target.oldPassword.value;
    var newPasswordConf = event.target.newPasswordConf.value;
    //Check if new passwords match
    if (newPassword !== newPasswordConf) {
      var message = "Passwords must match!";
      $('#changePasswordFailMessage').html(message);
      $('#changePasswordFailMessage').collapse('show');
      window.setTimeout(function(){
        $('#changePasswordFailMessage').collapse('hide');
      }, 2000);
    } else {
      //Change passwords if new passwords match
      Accounts.changePassword (oldPassword, newPassword, function (error) {
        if (error) {
          //console.log(error.reason);
          var message = "There was an error changing passwords: <strong>" + error.reason + ".</strong>";
          $('#changePasswordFailMessage').html(message);
          $('#changePasswordFailMessage').collapse('show');
          window.setTimeout(function(){
            $('#changePasswordFailMessage').collapse('hide');
          }, 2000);
        } else { //Show success on no error.
          $('#successResetPass').collapse('show');
          window.setTimeout(function(){
            $('#successResetPass').collapse('hide');
          }, 2000);
        };
      });
    }

  },
  //Forgot password event, sends email of registered user
  'submit #forgotPasswordForm' (event) {
    event.preventDefault();
    var emailResetDest = event.target.forgotEmail.value;
    Accounts.forgotPassword({email: emailResetDest}, function (error) {
      if (error) {
        var message = "There was an error sending the email: <strong>" + error.reason + ".</strong>";
        $('#forgotPasswordFormFail').html(message);
        $('#forgotPasswordFormFail').collapse('show');
        window.setTimeout(function(){
          $('#forgotPasswordFormFail').collapse('hide');
        }, 2000);
      } else {
        var message = "The email was sent!";
        $('#forgotPasswordFormSuccess').html(message);
        $('#forgotPasswordFormSuccess').collapse('show');
        window.setTimeout(function(){
          $('#forgotPasswordFormSuccess').collapse('hide');
        }, 2000);
      };
    });
  },
  //Signup form event
  'submit #signupForm' (event) {
    event.preventDefault();
    var formName = event.target.formName.value;
    var userName = event.target.inputUsername.value;
    var userEmail = event.target.inputEmail.value;
    var userPassword = event.target.inputPassword.value;
    var passwordConf = event.target.inputPasswordConf.value;
    var privacyCheck = event.target.privacyCheck.checked;
    var TOSCheck = event.target.TOSCheck.checked;
    if (privacyCheck == false || TOSCheck == false) {
      var message = "You <strong>must</strong> read and <strong>accept</strong> the Privacy Policy and the Terms of Use/Service Policy";
      $('#signUpFail').html(message);
      $('#signUpFail').collapse('show');
      window.setTimeout(function(){
        $('#signUpFail').collapse('hide');
      }, 4000);
    } else if (userPassword === passwordConf) {
      Accounts.createUser ({username: userName, email: userEmail, password: userPassword, profile: {name: formName}},
        function (error) {
          if (error) {
            var message = "There was an error signing up: <strong>" + error.reason + "</strong>";
            $('#signUpFail').html(message);
            $('#signUpFail').collapse('show');
            window.setTimeout(function(){
              $('#signUpFail').collapse('hide');
            }, 2000);
          } else {
            $('#signUpFail').collapse('hide');
          };
        }
      );
    } else {
      var message = "Passwords must match!";
      $('#signUpFail').html(message);
      $('#signUpFail').collapse('show');
      window.setTimeout(function(){
        $('#signUpFail').collapse('hide');
      }, 2000);
    }
  }
});
