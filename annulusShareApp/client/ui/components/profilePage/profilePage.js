import "./createModal.html";
import "./followUserModal.html";
import "./postTemplate.html";
import "./commentsModal.html";

import "./profilePage.html";
//Standard Import Style
import { Follower } from 'meteor/brewhk:follower';
import { Images } from '/collections/main.js';

//Initialize these vars for use between changeinput event and form submit
var uploadPath = undefined;
var uploadName = undefined;
var uploadLink = undefined;
var imageID = undefined;
var uploadfileObject = undefined;

/*
The folloing is a better way of handling error, with functions.
Too bad i didn't notice until it was too late. Maybe I'll convert all the error
handling to utilise this method. :(
*/

function animatedError(targetID, contentID, errorMessage) {
  var errorMessage = '<strong>Error: </strong>' + String(errorMessage);
  $(contentID).html(errorMessage);
  $(targetID).collapse('show');
  Meteor.setTimeout(function() {
    $(targetID).collapse('hide');
    $(contentID).html("");
  }, 4000);
};
function animatedSuccess(targetID, contentID, Message) {
  var Message = '<strong>Success! </strong>' + String(Message);
  $(contentID).html(Message);
  $(targetID).collapse('show');
  Meteor.setTimeout(function() {
    $(targetID).collapse('hide');
    $(contentID).html("");
  }, 4000);
};
function removeActiveClasses(idtoActivate) {
  $('#publicPostsTab').removeClass('active');
  $('#privatePostsTab').removeClass('active');
  $('#followingPostsTab').removeClass('active');
  $('#favouritePostsTab').removeClass('active');
  $(idtoActivate).addClass('active');
};

Template.uploadForm.helpers ({
  uploadedPosts() {
    if (Session.get("postView") == "publicPosts") {
      return Posts.find({}, {sort: {createdAt: -1}, limit: Session.get('pubViewLim'), });
    } else if (Session.get("postView") == "privatePosts") {
      return Posts.find({owner: Meteor.userId()}, {sort: {createdAt: -1,}, limit: Session.get('privViewLim'), });
    } else if (Session.get("postView") == "followingPosts") {
      return Posts.find({"owner": { "$in": Follower.getFollowingIds(Meteor.userId()) }}, {sort: {createdAt: -1,}, limit: Session.get('folViewLim'), });
    } else if (Session.get("postView") == "favouritePosts") {
      return Posts.find({_id: {"$in": Meteor.user().profile.favourites} }, {sort: {createdAt: -1,}, limit: Session.get('favViewLim'), });
    } else {
      return Posts.find({}, {sort: {createdAt: -1,}});
    }
  },
});

Template.commentsModal.helpers({
  currentPostID() {
    return Session.get('currentPost');
  },
  commentsExist() {
    if (Session.get('currentPost') != 0) {
      if (Posts.findOne({_id: Session.get('currentPost')}).comments) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  },
  commentsfromDB() {
    return Posts.findOne({_id: Session.get('currentPost')});
  },
  isOwnerofComment() {
    if (Session.get('currentPost') != 0) {
      if ((this.byuserID === Meteor.userId()) || (Roles.userIsInRole(Meteor.userId(), ['admin']))) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  },
});

Template.commentsModal.events({
  'submit #commentInput'(event, template) {
    event.preventDefault();
    var comment = event.target.commentInput.value;
    if (!Meteor.userId()) {
      $("#commentInput")[0].reset(); //Reset the form on error.
      animatedError('#searchError', '#searchErrorContent', ('Not logged in.'));
    } else {
      Meteor.call('addComment', comment,  Session.get('currentPost'), function (error) {
        if (error) {
          console.log("error.");
        }
      });
      $("#commentInput")[0].reset(); //Reset the form on error.
    }
  },
  'click #deleteComment' (event, template) {
    Meteor.call('deleteComment', this._id, Session.get('currentPost'));
  },
});

Template.postContents.helpers({
  isOwner() {
    if ((this.owner === Meteor.userId()) || (Roles.userIsInRole(Meteor.userId(), ['admin']))) {
      return true;
    } else {
      return false;
    }
  },
  isFollowing() {
    return Follower.checkIfFollowing(this.owner);
  },
  usernameisOwner() {
    var thisOwner = this.owner;
    if (thisOwner == Meteor.userId()) {
      return true;
    } else {
      return false;
    }
  },
  likes() {
    return Posts.findOne({_id: this._id}).likes;
  },
  ifLiked() {
    if (Meteor.user().profile.likedPosts) {
      if(Meteor.user().profile.likedPosts.indexOf(this._id) > -1) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  },
  ifFavourited() {
    if (Meteor.user().profile.favourites) {
      if(Meteor.user().profile.favourites.indexOf(this._id) > -1) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  },
});

Template.uploadForm.onCreated(function() {
  if (!Meteor.userId()) {
    Router.go('/');
  } else {
    Meteor.subscribe('posts.public'); //Subscribe to the post data to display
    Meteor.subscribe('brewhk:follower/followers', Meteor.userId()); // Subscribes to the list of followers by the user
    Meteor.subscribe('brewhk:follower/following', Meteor.userId()); // Subscribes to the list of users followed by the user
  }

  Template.uploadForm.events({
    'click #loadMoreLink'() {
      if (Session.get('postView') == "publicPosts") {
        Session.set('pubViewLim', Session.get('pubViewLim') + 5 );
      } else if (Session.get('postView') == "privatePosts") {
        Session.set('privViewLim', Session.get('privViewLim') + 5 );
      } else if (Session.get('postView') == "followingPosts") {
        Session.set('folViewLim', Session.get('folViewLim') + 5 );
      } else if (Session.get('postView') == "favouritePosts") {
        Session.set('favViewLim', Session.get('favViewLim') + 5 );
      }
    },
    'click #publicPostsTab'() {
      removeActiveClasses("#publicPostsTab");
      Session.set('postView', "publicPosts");
    },
    'click #privatePostsTab'() {
      removeActiveClasses("#privatePostsTab");
      Session.set('postView', "privatePosts");
    },
    'click #followingPostsTab'() {
      removeActiveClasses("#followingPostsTab");
      Session.set('postView', "followingPosts");
    },
    'click #favouritePostsTab'() {
      removeActiveClasses("#favouritePostsTab")
      Session.set('postView', "favouritePosts");
    },
    'click #postDelete'() {
      Meteor.call('posts.remove', this._id);
    },
    'click #favourite'() {
      Meteor.call('addFavourite', this._id);
    },
    'click #unfavourite'() {
      Meteor.call('removeFavourite', this._id);
    },
    'click #likeButton'() {
      Meteor.call('addLike', this._id);
    },
    'click #unlikeButton'() {
      Meteor.call('removeLike', this._id);
    },
    'click #commentModalButton' (event,template) {
      Session.set('currentPost', this._id);
    },
    'click #followUserLink'(event, template) {
      var username = this.username;
      Meteor.call('followUser', username);
    },
    'click #unfollowUserLink'(event, template) {
      var username = this.username;
      Meteor.call('unfollowUser', username);
    },
    'submit #searchForm'(event, template) {
      event.preventDefault();
      var username = event.target.username.value;
      if (!Meteor.userId()) {
        $("#searchForm")[0].reset(); //Reset the form on error.
        animatedError('#searchError', '#searchErrorContent', ('Not logged in.'));
      } else if (!username) {
        animatedError('#searchError', '#searchErrorContent', 'Input a username.');
      } else {
        Meteor.call('followUser', username, function (error) {
          if (error) {
            animatedError('#searchError', '#searchErrorContent', ("User " +'"'+username+'"'+ " not found.")); //Nice looking error
          } else {
            animatedSuccess('#searchSuccess', '#searchSuccessContent', ("You're now following " + username + ".")); //Here too
          }
        });
        $("#searchForm")[0].reset(); //Reset the form on error.
      }
    },
    'submit #unfollowForm'(event, template) {
      event.preventDefault();
      var username = event.target.unfollowUsername.value;
      if (!Meteor.userId()) {
        $("#unfollowForm")[0].reset(); //Reset the form on error.
        animatedError('#unfollowError', '#unfollowErrorContent', ('Not logged in.'));
      } else if (!username) {
        animatedError('#unfollowError', '#unfollowErrorContent', 'Input a username.');
      } else {
        Meteor.call('unfollowUser', username, function (error) {
          if (error) {
            animatedError('#unfollowError', '#unfollowErrorContent', ("User " +'"'+username+'"'+ " not found.")); //Nice looking error
          } else {
            animatedSuccess('#unfollowSuccess', '#unfollowSuccessContent', ("You're no longer following " + username + ".")); //Here too
          }
        });
        $("#unfollowForm")[0].reset(); //Reset the form on error.
      }
    },
    'click #unfollowALL'(event, template) {
      if (!Meteor.userId()) {
        animatedError('#unfollowError', '#unfollowErrorContent', ('Not logged in.'));
      }
      else {
        Follower.unfollowAll( function (error, result) {
          if (error) {
            animatedError('#unfollowError', '#unfollowErrorContent', (error)); //Nice looking error
          } else {
            animatedSuccess('#unfollowSuccess', '#unfollowSuccessContent', ("You're no longer following anyone.")); //Here too
          }
        });
      }
    },
    'submit #postForm'(event, template) {
      event.preventDefault();
      var title = event.target.title.value;
      var description = event.target.description.value;
      if (!Meteor.userId()) {
        $("#postForm")[0].reset(); //Reset the form on error.
        $('#preUploadErrorContent').html("<strong>Error: </strong>Please log in to post.");
        $('#preUploadError').collapse('show');
        Meteor.setTimeout(function() {
          $('#preUploadError').collapse('hide');
          $('#preUploadErrorContent').html("");
        }, 4000);
      } else {
        if (!uploadPath) { //Check if an image was uploaded
          //alert("Please upload an image first, then submit your post.")
          $('#missingImageError').collapse('show');
          Meteor.setTimeout(function() {
            $('#missingImageError').collapse('hide');
          }, 2500);
        } else {
          try {
            Meteor.call('uploadFormExif', uploadPath, uploadName, uploadLink, title, description, imageID, Follower.getFollowerIds(Meteor.userId()), function(error) {
              if (error) {
                animatedError(preUploadError, preUploadErrorContent, "Error extracting EXIF. Please upload image with EXIF.");
                Meteor.call('image.remove', imageID);
                imageID = undefined;
              }
            });
          } catch (e) {
            template.find("form").reset();
            $('#preUploadErrorContent').html(e); //Change the html of the error element
            $('#preUploadError').collapse('show');  //Reveal the error
            Meteor.setTimeout(function() { //Wait a bit
              $('#preUploadError').collapse('hide'); //Hide the error
              $('#preUploadErrorContent').html(""); //Clear the html for use by other errors
              errMessage = ""; //Clear the var
            }, 4000);
          }

          //Clean up.
          //Reset the vars so it can be used agian by the change #fileInput
          uploadPath = undefined;
          uploadName = undefined;
          uploadLink = undefined;
          $("#postForm")[0].reset(); //Reset the form on error.
          Meteor.setTimeout(function() {
            $('#uploadingFileSuccess').collapse('hide');
          }, 100);
        }
      }
    },
    'change #fileInput'(e, template) {
      if (e.currentTarget.files && e.currentTarget.files[0]) {
        // We upload only one file, in case
        // multiple files were selected
        const upload = Images.insert({
          file: e.currentTarget.files[0],
          streams: 'dynamic',
          chunkSize: 'dynamic'
        }, false);

        upload.on('start', function () {
          $('#uploadingFileLoading').collapse('show');
        });

        upload.on('end', function (error, fileObj) {
          if (error) {
            $("#postForm")[0].reset(); //Reset the form on error.

            var serverError = String(error).slice(6); //error val already includes "error:" so we need to remove it to make it <strong>
            var errMessage = "<strong>Error: </strong>" + serverError; //Construct a nice looking error

            $('#preUploadErrorContent').html(errMessage); //Change the html of the error element
            $('#preUploadError').collapse('show');  //Reveal the error

            Meteor.setTimeout(function() { //Wait a bit
              $('#preUploadError').collapse('hide'); //Hide the error
              $('#preUploadErrorContent').html(""); //Clear the html for use by other errors
              errMessage = ""; //Clear the var
            }, 4000);

          } else {
            //alert('File "' + fileObj.name + '" successfully uploaded');
            Meteor.setTimeout(function() {
              $('#uploadingFileLoading').collapse('hide');
            }, 1000);
            Meteor.setTimeout(function() {
              $('#uploadingFileSuccess').collapse('show');
            }, 2000);
            uploadPath = fileObj.path;
            uploadName = fileObj.name;
            uploadLink = Images.link(fileObj);
            imageID = fileObj._id;
            uploadfileObject = fileObj;
            //Meteor.call('uploadFormExif', fileObj.path, title, description);
          }
        });

        upload.start();

        upload.on('afterUpload', function(fileRef) {
          // Run `createThumbnails` only over PNG, JPG and JPEG files
          if (/png|jpe?g/i.test(fileRef.extension || '')) {
            createThumbnails(this, fileRef, (error, fileRef) => {
              if (error) {
                console.error(error);
              }
            });
          }
        });
      }
    },
  });

});
