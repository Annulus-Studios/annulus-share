import './home.html';
import ScrollReveal from 'scrollreveal';

//Set session variable when on resetPasswordToken activated this is for
//Resseting the password from email
if (Accounts._resetPasswordToken) {
  Session.set('resetPasswordToken', Accounts._resetPasswordToken);
}

//Helpers
Template.page_Home.helpers({
  //Set variable to tell DOM if resetting password
  resetPassword: function() {
    return Session.get('resetPasswordToken');
  }
});

Template.page_Home.onRendered(function() {
  var headerSize = $(".homeHeader").outerHeight();
  //Kill modals on render (to fix when users have a modal open and press back or forward)
  $('body').removeClass('modal-open');
  $('.modal-backdrop').remove();
  setTimeout(function () {
    $("#homepageContentLeft").addClass("revealContent");
    $("#homepageContentRight").addClass("revealContent");
  }, 1000);


  window.sr = ScrollReveal(); //Initialize Scrollreveal

  sr.reveal('#cardContentLeft', { origin: 'left', duration: 1000, delay: 200 });
  sr.reveal('#cardContentMiddle', { origin: 'bottom', duration: 1000, delay: 400 });
  sr.reveal('#cardContentRight', { origin: 'right', duration: 1000, delay: 600 });
});
