// Client entry point, imports all client code
import { Template } from 'meteor/templating';
import { Accounts } from 'meteor/accounts-base';
import { FilesCollection } from 'meteor/ostrio:files';
import { jquery } from 'meteor/jquery';

//Import client side JS
import './routes.js';
import '/collections/main.js';

Meteor.startup(() => {
  PushNotification.createChannel(
      () => {
          console.log('createChannel');
      },
      () => {
          console.log('error');
      },
      {
         id: Meteor.userId(), //Use any Id you prefer, but the same Id for this channel must be sent from the server,
         description: 'Android Channel', //And any description your prefer
         importance: 3,
         vibration: true
        }
  );
  Push.Configure({
    android: {
      senderID: 659123908175,
      alert: true,
      badge: true,
      sound: true,
      vibrate: true,
      clearNotifications: true,
      icon: 'logo',
      iconColor: '#4cae4c',
    },
  });
});
