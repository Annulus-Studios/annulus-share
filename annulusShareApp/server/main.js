//Import some required libraries
import { Images } from '/collections/main.js'; //Need to manually import the Images collection
                                               //as it is not a normal Meteor collection.
const exif = require("aj-jpeg-exif");          //Rquire the exif reader library

// Import all server code
import './init.js';
import './publishes.js';
import './methods.js';
import './exifFormMethod.js';