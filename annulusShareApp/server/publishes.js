Meteor.publish('posts.public', function(limit) {
  var dl = limit || 10;
  return Posts.find({}, {
    //Fields to publish
    fields: { //For security reasons, only publish these fields (which should be all of them) prevents accidental publish of secret data
      createdAt : 1,
      title : 1,
      description : 1,
      url : 1,
      fileName: 1,
      fileLink: 1,
      thumbnailLink: 1,
      owner: 1,
      username: 1,
      metadata: 1,
      likes: 1,
      likedBy: 1,
      comments: 1,
    }
  });
});
