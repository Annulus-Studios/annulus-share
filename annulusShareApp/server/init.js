Meteor.startup(() => {
  //SET EMAIL VARIABLES
  Accounts.emailTemplates.siteName = "Annulus-Share";
  Accounts.emailTemplates.from = "AnnulusShare Admin <accounts@annulus-share.com>";
  Accounts.emailTemplates.enrollAccount.subject = function (user) {
      return "Welcome to Annulus-Share, " + user.profile.name;
  };
  Accounts.emailTemplates.enrollAccount.text = function (user, url) {
     return "You have been selected to participate in building a better future!"
       + " To activate your account, simply click the link below:\n\n"
       + url;
  };
  Accounts.emailTemplates.resetPassword.from = function () {
     // Overrides value set in Accounts.emailTemplates.from when resetting passwords
     return "Annulus-Share Password Reset <no-reply@annulus-studios.com>";
  };

  Push.debug = true;
  Push.Configure({
    gcm: {
      apiKey: 'AAAAmXbYrk8:APA91bFRtpnAgvoZjbbaGNearH1_Sl1R63ch5iiT7ZPCN1giqAEEZIdYsciYEpjd-jp5anWeZm1GLfnAThfV9puX_B2a-lAavXj0DjTlWDaJOzUonFiGsaoaX12dGSkS-l_JAbP9MbMe',
      projectNumber: 659123908175
    }
    // production: true,
    // 'sound' true,
    // 'badge' true,
    // 'alert' true,
    // 'vibrate' true,
    // 'sendInterval': 15000, Configurable interval between sending
    // 'sendBatchSize': 1, Configurable number of notifications to send per batch
    // 'keepNotifications': false,
  //
  });
  Push.send({
  	  from: 'test',
  	  title: 'test',
  	   text: 'hello',
            android_channel_id:this.userId,		//The android channel should match the id on the client
            query: {
                userId: this.userId
            },
            gcm: {
              style: 'inbox',
              summaryText: 'There are %n% notifications'
            },
  });
});
