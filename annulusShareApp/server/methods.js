import { Images } from '/collections/main.js';
const exif = require("aj-jpeg-exif");
//Methods
Meteor.methods({
  'posts.remove'(postId) {
    check(postId, String);
    if ( (this.userId === Posts.findOne({_id: postId}).owner) || (Roles.userIsInRole(this.userId, ['admin'])) ) {
      var imageID = Posts.findOne({_id: postId}).metadata.imageID; //Get the imageID using the link between Images and Posts Collections
      Images.remove({_id: imageID}); //This will delete the Images collection entry and delete the img file itself.
      Posts.remove({_id: postId}); //This will delete the post from Posts collection
    } else {
      throw new Meteor.Error('Unauthorized, please log-in.');
    }
  },
  'image.remove'(imageID) { //For server use only, not called by the cleint.
    check(imageID, String);
    var imageOwner = Images.findOne(imageID).userId;
    if ( (this.userId === imageOwner) || (Roles.userIsInRole(this.userId, ['admin'])) ) {
      Images.remove({_id: imageID}); //This will delete the Images collection entry and delete the img file itself.
    } else {
      throw new Meteor.Error('Unauthorized, please log-in.');
    }
  },
  'followUser' (username) {
    check(username, String);
    if (!this.userId) {
      throw new Meteor.Error('Unauthorized, please log-in.');
    } else {
      var userId = Meteor.users.findOne({username: username})._id;
      Meteor.call('brewhk:follower/follow', userId);
    }
  },
  'unfollowUser' (username) {
    check(username, String);
    if (!this.userId) {
      throw new Meteor.Error('Unauthorized, please log-in.');
    } else {
      var userId = Meteor.users.findOne({username: username})._id;
      Meteor.call('brewhk:follower/unfollow', userId);
    }
  },
  'addComment' (comment, postId) {
    check(comment, String);
    check(postId, String);
    if (!this.userId) {
      throw new Meteor.Error('Unauthorized, please log-in.');
    } else {
      var date = new Date();
      Posts.update({_id: postId}, {$push: {"comments": {
        _id : Random.id(),
        byuserName: Meteor.user().username,
        byuserID: this.userId,
        comment: comment,
        commentedAt: date.toString(),
      }} }, {upsert: true});

      Push.send({ from: 'Annulus Share', title: 'New Comment', text: 'User ' + Meteor.user().username + ' just commented on your post!', badge: 12, query: { userId: Posts.findOne({_id: postId}).owner, } });
    }
  },
  'deleteComment' (commentIDClient, postIDClient) {
    //Meteor does mongo find() differently than Mongo shell, this one feature killed me for hours. -_-
    check(commentIDClient, String);
    check(postIDClient, String);
    var userIDfromSearch = Posts.findOne({"comments._id":commentIDClient}, {fields: {_id:0, "comments.$": 1}}).comments[0].byuserID;
    if (!this.userId) {
      throw new Meteor.Error('posts.deletecomment', 'Unauthorized, please log-in.');
    } else if ( (this.userId === userIDfromSearch) || (Roles.userIsInRole(this.userId, ['admin']))) {
      Posts.update({_id: postIDClient}, {$pull: {comments: {_id: commentIDClient }}})
    } else {
      throw new Meteor.Error('posts.deletecomment', 'Unauthorized, Not your comment.');
    }
  },
  'addFavourite' (postId) {
    check(postId, String);
    if (Meteor.users.findOne({_id: this.userId}).profile.favourites == undefined) {
      Meteor.users.update({_id: this.userId}, {$push: {"profile.favourites": postId} }, {upsert: true});
    } else {
      var alreadyFavourite = Meteor.users.findOne({_id: this.userId}).profile.favourites;
      if ( alreadyFavourite.indexOf(postId) == -1) { //Only add favourite if user is not favourite the post already
          Meteor.users.update({_id: this.userId}, {$push: {"profile.favourites": postId} }, {upsert: true});
      }
    }
  },
  'removeFavourite' (postId) {
    check(postId, String);
    if (Meteor.users.findOne({_id: this.userId}).profile.favourites == undefined) {
      Meteor.users.update({_id: this.userId}, {$push: {"profile.favourites": []} }, {upsert: true});
    } else {
      var alreadyFavourite = Meteor.users.findOne({_id: this.userId}).profile.favourites;
      if ( alreadyFavourite.indexOf(postId) != -1) { //Only remove favourite if user is already favourite the post
          Meteor.users.update({_id: this.userId}, {$pull: {"profile.favourites": postId} } );
      }
    }
  },
  'addLike' (postId) {
    check(postId, String); //Just checking
    var userId = this.userId;
    if (Meteor.users.findOne({_id: this.userId}).profile.likedPosts == undefined) {
      Meteor.users.update({_id: this.userId}, {$push: {"profile.likedPosts": postId} }, {upsert: true});
      Posts.update({_id: postId}, {$inc: { likes: 1 } });
      Posts.update({_id: postId}, {$push: { likedBy: this.userId } }, {upsert: true});
      Push.send({ from: 'Annulus Share', title: 'New Post', text: 'User ' + Meteor.user().username + ' just like your post!', badge: 12, query: { userId: Posts.findOne({_id: postId}).owner, } });
    } else {
      var alreadyLiked = Meteor.users.findOne({_id: this.userId}).profile.likedPosts; //Get the array of likedPosts
      if ( alreadyLiked.indexOf(postId) == -1 ) { //This will only add a like if the user has not liked the post before.
        Meteor.users.update({_id: this.userId}, {$push: {"profile.likedPosts": postId} }, {upsert: true});
        Posts.update({_id: postId}, {$inc: { likes: 1 } });
        Posts.update({_id: postId}, {$push: { likedBy: userId } }, {upsert: true});
        Push.send({ from: 'Annulus Share', title: 'New Post', text: 'User ' + Meteor.user().username + ' just like your post!', badge: 12, query: { userId: Posts.findOne({_id: postId}).owner, } });
      }
    }
  },
  'removeLike' (postId) {
    check(postId, String); //Just checking
    if (Meteor.users.findOne({_id: this.userId}).profile.likedPosts == undefined) {
      Meteor.users.update({_id: this.userId}, {$push: {"profile.likedPosts": []} }, {upsert: true});
    } else {
      var alreadyLiked = Meteor.users.findOne({_id: this.userId}).profile.likedPosts; //Get the array of likedPosts
      if ( alreadyLiked.indexOf(postId) != -1 ) { //This will only remove a like if the user has liked the post before.
        Meteor.users.update({_id: this.userId}, {$pull: {"profile.likedPosts": postId} } );
        Posts.update({_id: postId}, { $inc: { likes: -1 }});
        Posts.update({_id: postId}, {$pull: { likedBy: this.userId } });
      }
    }
  },
});
