import { Images } from '/collections/main.js';
import { Follower } from 'meteor/brewhk:follower';
const exif = require("aj-jpeg-exif");
//This method is huge so I'm giving it its own file.
Meteor.methods({
  'uploadFormExif'(imagePath, imageName, imageLink, postTitle, postDescription, imageID, followersArray) { //exif information extraction called on successful file upload.
    check(postTitle, String);
    check(postDescription, String);
    check(imagePath, String);
    check(imageName, String);
    check(imageLink, String);
    check(imageID, String);

    // Make sure the user is logged in before inserting a post
    if (!Meteor.userId()) {
      throw new Meteor.Error('Unauthorized, please log-in.');
    }


    var thumbnailLinkUnsec = Images.findOne({_id: imageID}).link('thumbnail');
    var imageLinkUnsec = Images.findOne({_id: imageID}).link();

    //Need to change the file links to show hTTPS not HTTP to avoid errors, there is no (at the time of writing)
    //to do this with solely the package. Slicing the string will work though.
    //to get images to work properly is development, let finalLink = imageLink.
    function httptohttps(linktoChange) {

      var finalLink;

      var insec = linktoChange.slice(0,5);

      if (insec === "https") {
        finalLink = linktoChange;  //For some reason, if its already https, leave it alone
      } else if (insec === "http:") {     //If the link is http, convert to https
        var rest = linktoChange.slice(4);
        var finalLink = "https" + (rest);
      } else {
        finalLink = linktoChange;   //Default to the normal link
      }
      //finalLink = imageLink;  //For development use only
      if (Meteor.isDevelopment) {
        finalLink = linktoChange;
        console.log("In development so no HTTPS needed. Normal links served.");
      }

      return finalLink;
    }

    //Run the function to appropriately update links.
    var thumbnailFinalLink = httptohttps(thumbnailLinkUnsec);
    var imageFinalLink = httptohttps(imageLinkUnsec);


    //Extract EXIF metadata and let it equal to exifOutput
    var exifOutput;
    try {
      exifOutput = exif.parseSync(imagePath);
    } catch(err) {
      throw new Meteor.Error('Error reading EXIF information from image.');
    }

    var flashVal; //Initiate var
    var oldFlashVal = String(exifOutput.SubExif.Flash); //Get raw info from Exif extraction

    //Parsing flash data, this is a large switch to account for different flash data.
    switch(oldFlashVal) {
      case "0":
        flashVal = "No flash.";
        break;
      case "1":
        flashVal = "Fired";
        break;
      case "5":
        flashVal = "Fired, Return not detected.";
        break;
      case "7":
        flashVal = "Fired, Return detected.";
        break;
      case "8":
        flashVal = "On, Did not fire.";
        break;
      case "9":
        flashVal = "On, Fired.";
        break;
      case "13":
        flashVal = "On, Return not detected.";
        break;
      case "15":
        flashVal = "On, Return detected.";
        break;
      case "16":
        flashVal = "Off, Did not fire.";
        break;
      case "20":
        flashVal = "Off, Did not fire, Return not detected.";
        break;
      case "24":
        flashVal = "Auto, Did not fire.";
        break;
      case "25":
        flashVal = "Auto, Fired";
        break;
      case "29":
        flashVal = "Auto, Fired, Return not detected.";
        break;
      case "31":
        flashVal = "Auto, Fired, Return detected.";
        break;
      case "32":
        flashVal = "No flash function.";
        break;
      case "48":
        flashVal = "Off, No flash function.";
        break;
      case "65":
        flashVal = "Fired, Red-eye reduction.";
        break;
      case "69":
        flashVal = "Fired, Red-eye reduction, Return not detected.";
        break;
      case "71":
        flashVal = "Fired, Red-eye reduction, Return detected";
        break;
      case "73":
        flashVal = "On, Red-eye reduction";
        break;
      case "77":
        flashVal = "On, Red-eye reduction, Return not detected";
        break;
      case "79":
        flashVal = "On, Red-eye reduction, Return detected";
        break;
      case "80":
        flashVal = "Off, Red-eye reduction";
        break;
      case "88":
        flashVal = "Auto, Did not fire, Red-eye reduction";
        break;
      case "89":
        flashVal = "Auto, Fired, Red-eye reduction";
        break;
      case "93":
        flashVal = "Auto, Fired, Red-eye reduction, Return not detected";
        break;
      case "95":
        flashVal = "Auto, Fired, Red-eye reduction, Return detected";
        break;
      default:
        flashVal = "Not found.";
    }

    //Defaulting to "Not found" Couldn't figure out how to do this with a loop so I'll do it like this:
    //Start by initializing the values.
    var parsedExif = {
      exMake: exifOutput.Make,
      exModel: exifOutput.Model,
      exLens: exifOutput.SubExif.LensModel,
      exCopyright: exifOutput.Copyright,
      exExposure: exifOutput.SubExif.ExposureTime,
      exISO: exifOutput.SubExif.PhotographicSensitivity,
      exAperture: exifOutput.SubExif.FNumber,
      exDate: exifOutput.DateTime,
      exResX: exifOutput.XResolution,
      exResY: exifOutput.YResolution,
      imagePixelResX: exifOutput.SubExif.PixelXDimension,
      imagePixelResY: exifOutput.SubExif.PixelYDimension,
    }
    //Then check each with an if statement and set it to "Not found" when returned blank from EXIF reader.
    //Doing this makes the code long but it works well.
    if (parsedExif.exMake == undefined) {
      parsedExif.exMake = "Not found.";
    }
    if (parsedExif.exModel == undefined) {
      parsedExif.exModel = "Not found.";
    }
    if (parsedExif.exLens == undefined) {
      parsedExif.exLens = "Not found.";
    }
    if (parsedExif.exCopyright == undefined) {
      parsedExif.exCopyright = "Not found.";
    }
    if (parsedExif.exExposure == undefined) {
      parsedExif.exExposure = "Not found.";
    } else {
      parsedExif.exExposure = parsedExif.exExposure + "(s)"; //Add "seconds"
    }
    if (parsedExif.exISO == undefined) {
      parsedExif.exISO = "Not found.";
    }
    if (parsedExif.exAperture == undefined) {
      parsedExif.exAperture = "Not found.";
    } else {
      parsedExif.exAperture = "f/" + parsedExif.exAperture;
    }
    if (parsedExif.exDate == undefined) {
      parsedExif.exDate = "Not found.";
    }
    if (parsedExif.exResX == undefined) {
      parsedExif.exResX = "Not found.";
    } else {
      parsedExif.exResX = parsedExif.exResX + " pixels/inch";
    }
    if (parsedExif.exResY == undefined) {
      parsedExif.exResY = "Not found.";
    }
    if (parsedExif.imagePixelResX == undefined) {
      parsedExif.imagePixelResX = "(N/A)";
    }
    if (parsedExif.imagePixelResY == undefined) {
      parsedExif.imagePixelResY = "(N/A)";
    }


    Posts.insert({
      createdAt : new Date(),
      title : postTitle,
      description : postDescription,
      url : imagePath,
      fileName: imageName,
      fileLink: imageFinalLink,
      thumbnailLink: thumbnailFinalLink,
      owner: Meteor.userId(),
      username: Meteor.user().username,
      metadata: {
      	make: parsedExif.exMake,
      	model: parsedExif.exModel,
        lens: parsedExif.exLens,
      	copyright: parsedExif.exCopyright,
      	exposure: parsedExif.exExposure,
      	iso: parsedExif.exISO,
      	aperture: parsedExif.exAperture,
      	flash: flashVal,
      	date: parsedExif.exDate,
      	width: parsedExif.exResX,
      	height: parsedExif.exResY,
        imageID: imageID,
        pixelResX: parsedExif.imagePixelResX,
        pixelResY: parsedExif.imagePixelResY,
      },
      likes: 0,
      likedBy: [],
    });

    Push.send({
      from: 'Annulus Share',
      title: 'New Post',
      text: 'User ' + Meteor.user().username + ' just made a new post!',
      badge: 12,
      // sound: fileInPublicFolder
      query: {
        userId: {$in: followersArray},
      }
    });
  },
});
