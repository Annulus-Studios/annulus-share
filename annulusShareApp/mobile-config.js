// This section sets up some basic app metadata, the entire section is optional.
App.info({
  id: 'com.annulus_studios.annulus_share',
  name: 'Annulus Share',
  description: 'Photo sharing and image metadata analysis.',
  author: 'Annulus Studios',
  email: 'annulus.studios@gmail.com',
  website: 'https://app.ajsystems.tech/',
  version: '0.5.0',
  buildNumber: '120'
});
// Set up resources such as icons and launch screens.
App.icons({
  'android_mdpi': 'androidAssets/icons/android/icon-48-mdpi.png',
  'android_hdpi': 'androidAssets/icons/android/icon-72-hdpi.png',
  'android_xhdpi': 'androidAssets/icons/android/icon-96-xhdpi.png',
  'android_xxhdpi': 'androidAssets/icons/android/icon-144-xxhdpi.png',
  'android_xxxhdpi': 'androidAssets/icons/android/icon-192-xxxhdpi.png',
  // More screen sizes and platforms...
});
App.launchScreens({
  'android_mdpi_portrait': 'androidAssets/splashes/android/screen-mdpi-portrait.png',
  'android_mdpi_landscape': 'androidAssets/splashes/android/screen-mdpi-landscape.png',
  'android_hdpi_portrait': 'androidAssets/splashes/android/screen-hdpi-portrait.png',
  'android_hdpi_landscape': 'androidAssets/splashes/android/screen-hdpi-landscape.png',
  'android_xhdpi_portrait': 'androidAssets/splashes/android/screen-xhdpi-portrait.png',
  'android_xhdpi_landscape': 'androidAssets/splashes/android/screen-xhdpi-landscape.png',
  // More screen sizes and platforms...
});
// Set PhoneGap/Cordova preferences.
App.setPreference('HideKeyboardFormAccessoryBar', true);
App.setPreference('Orientation', 'default');

//Push Notifications
App.configurePlugin('phonegap-plugin-push', {
  SENDER_ID: 659123908175
});

App.accessRule("*");
