# Annulus Share - Project Intentions

I intend to produce an interactive and dynamic website to allow photography enthusiasts/hobbyists to not only share their work but also share HOW they achieved such a photo. This will be done by extracting critical metadata from either JPEG or RAW (CR2, NEF, etc.) then compiling and presenting that information in a streamlined and design-driven fashion.

I intend to produce a competitive product that will provide an alternative to Flickr. I intend for my project to stand out against Flickr by producing a more visually engaging and pleasing design. By carefully following elements and principles of design, as well as extensive testing, I intend to produce a visually superior product while protecting user’s data not distributing information with other companies, organisations, and/or individuals. 

As a complement to the website, I intend to produce a video outlining and exploring how to utilise the website functions efficiently. This video will be posted onto a separate page which overall contains information about the website and its specific functions. The video is intended to go into detail about metadata tags and filetypes. This will help other photographers to improve their skills and quality of photos by exposing the benefits of RAW photography and the specific settings that coincide with such photos. 

The project is intended to allow users to share photographs taken from cameras ranging from high-end DSLR’s to smartphone cameras. Then to display that information in a cohesive and user-friendly manner which will allow people of all skill levels to be able to relate and compare to their own photos. 

I intend to utilise my skills in front-end web design to achieve this as well as using libraries and frameworks such as Bootstrap and SCSS/SASS to make the CSS and web design element more efficient and quicker to produce with relative speed. Furthermore, back-end technologies such as NGINX, Letsencrypt TLS, HTTP2, and Cloudflare to produce a modern, secure, and cohesive user-experience which will give the consumer a level of comfort in knowing that their data is handled with care and efficiency. I also intend to use MeteorJS as the underlying platform for my application as it utilises a language that I already know (Javascript).

I intend to utilise as much feedback as possible from my peers, other photography enthusiasts, and my mentors and teachers. The feedback received will greatly aid the development and production process as it will allow me to produce a website that satisfies a wide range of users. The project is intended to take until the beginning of the third term of the school year in 2017.  By this time, it is intended that the project will be finalised and completely ready for production.


## Built With

* [MeteorJS](https://www.meteor.com/) - Underlying full-stack platform
* [Bootstrap v4](https://v4-alpha.getbootstrap.com/) - Web framework


## Authors

* **Arian Jahiri** - *Initial and all work* - [ArianTech](https://ait-archive.com/)


## License

This project is licensed under the GNU GPLv2 License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Ben Jones - Teacher and mentor.
